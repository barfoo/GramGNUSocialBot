### This bot can send and recibe quips from gnusocial network, using the telegram-bot api

# Requeriments:  
python-pycurl
python telegram-bot-api

# For installing telegram bot api:    
pip install python-telegram-bot --upgrade

#configuration:  
Edit telegrambot.cfg and gnusocialbot.cfg    
Before the bot can send quips to a telegram group, use /setPrivacy false with botfather   
For know the group id uncomment to lines in the code, if you dont know it, after apoint it, comment them again   

# Usage:
python  GramGNUSocialBot.py
