from StringIO import StringIO
import pycurl
import json
import ConfigParser 
try:
    # python 3
    from urllib.parse import urlencode
except ImportError:
    # python 2
    from urllib import urlencode
    
class GNUSocialBot():
     
    def __init__(self):
        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read("gnusocialbot.cfg")
        self.socialname = self.cfg.get("social", "user")
        self.socialpass = self.cfg.get("social", "password")
        self.socialnode = self.cfg.get("social", "node")
        self.url_status = "https://" + self.socialnode + "/api/statuses/friends.json"
        self.url_update = "https://" + self.socialnode + "/api/statuses/update.xml" 
        
            
    def news(self):
        try:
            buffer = StringIO()
            curl = pycurl.Curl()
            curl.setopt(pycurl.SSL_VERIFYPEER, False)
            curl.setopt(pycurl.SSL_VERIFYHOST, False)
            curl.setopt(pycurl.USERPWD, self.socialname + ":" + self.socialpass)
            curl.setopt(curl.URL, self.url_status)
            curl.setopt(pycurl.WRITEFUNCTION, buffer.write)
            curl.perform()
            curl.close()
            data = json.loads(buffer.getvalue())
            return(data)
        except Exception as e:
            print(e)
            return None
        
           
    def social_status(self, msg):
        try:
            curl = pycurl.Curl()
            curl.setopt(pycurl.SSL_VERIFYPEER, False)
            curl.setopt(pycurl.SSL_VERIFYHOST, False)
            curl.setopt(pycurl.USERPWD, self.socialname + ":" + self.socialpass)
            curl.setopt(pycurl.POST, 1)
            post_data = {"status" : msg}
            postfields = urlencode(post_data)
            curl.setopt(pycurl.POSTFIELDS, postfields)
            curl.setopt(curl.URL, self.url_update)
            curl.perform()
            curl.close()
        except Exception as e:
            # print(e)
            # sleep(60)
            pass
