import telegram
import ConfigParser 
from threading import Thread
import threading
from GNUSocialBot import GNUSocialBot
from datetime import datetime
from StringIO import StringIO
from queue import Queue
from telegram.error import NetworkError, Unauthorized
from time import sleep
from sys import argv
from sys import exit
try:
    # python 3
    from urllib.parse import urlencode
except ImportError:
    # python 2
    from urllib import urlencode

class TelegramBot():
    def __init__(self):
        self.groups = []
        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read("telegrambot.cfg")
        self.token = self.cfg.get("bot", "token")
        self.group = self.cfg.get("bot", "group")
        self.tgbot = telegram.Bot(self.token)
        self.socialbot = GNUSocialBot()
        t = Thread(target=self.read_tg)
        t.start()
        print('Please wait a couple of minutes until it\'s correctly '
                  'connected')
        self.olds = []      
        
    def sendnews(self):
        news = self.socialbot.news()
        try:
            for item in news:
                if item['status']['text'] not in self.olds:
                    aux = '' + item['screen_name'] + "-> "
                    msg = item['status']['text']
                    aux = aux + msg
                    try:
                        #looking for good parsing? im waiting......
                        #self.tgbot.sendMessage(parse_mode='HTML', self.group, aux.encode('utf-8'))
                        self.tgbot.sendMessage(self.group, aux.encode('utf-8'))
                        self.olds.append(item['status']['text'])
                        sleep(2)
                    except Exception as e:
                        try:
                            #self.tgbot.sendMessage(parse_mode='HTML', self.group, aux.encode('utf-8'))
                            self.tgbot.sendMessage(chat_id=chat_id, text=aux, parse_mode=telegram.ParseMode.HTML)
                            self.olds.append(item)
                            sleep(2)
                        except Exception as e:
                            pass
                        #print(e)
                        pass
                        
        except Exception as e:
            print(e)
            pass        
        
    def read_tg(self):
        datenow = datetime.now()
        update_id = 0
        old_updates = [] 
        mycounter = 0
        try:
            while 1:
                if mycounter > 120:
                    mycounter = 0
                    self.sendnews()
                mycounter += 1
                #print(mycounter)
                for update in self.tgbot.getUpdates(offset=update_id,
                                                      timeout=10):
                    if update is not None:
                        message = update.message.text
                        #uncommnet this two lines for cacht the chat_id
                        #chat_id = update.message.chat_id
                        #print(chat_id)
                        # sometimes there's no user. weird, but it happens
                        msg_date = update.message.date
                        if message and message not in old_updates:
                            min_diff = (datenow - msg_date).total_seconds()                                                                                                              
                            if (min_diff < 600):                                                                                                                                         
                                self.socialbot.social_status(message.encode('utf-8'))
                                old_updates.append(message)

                            
                
        except NetworkError as e:
            print(e)
            sleep(1)

        except Unauthorized:
            print(e)
            sleep(1)

        except Exception as e:
            print(e)
            sleep(1)


if __name__ == '__main__':
    telegrambot = TelegramBot()

    


